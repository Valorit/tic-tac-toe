let nodes = document.getElementsByClassName('node');
let player = document.getElementById('player');
let count = document.getElementById('roundCount');
const reset = document.getElementById('reset');
const playerName1 = 'Player 1 (X)';
const playerName2 = 'Player 2 (O)';
let player1 = true;
let player2 = false;
let isWin = false;
let moves = 0;

const map = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

const resetClick = () => {
  document.location.reload();
};

const endGame = () => {
  setTimeout(() => {
    if (isWin) {
      alert(`${player1 ? playerName2 : playerName1} Win!`);
    } else if (isWin === false && moves === 9) {
      alert('Draw! Try again.');
    }
    resetClick();
  }, 100);
};

const colorWin = (idMap) => {
  for (let z = 0; z < idMap.length; z++) {
    const id = idMap[z];
    for (let n = 0; n < nodes.length; n++) {
      const element = nodes[n];
      if (element.id === id) {
        element.style.backgroundColor = 'orange';
      }
    }
  }
};

const winCheck = () => {
  for (let i = 0; i < map.length; i++) {
    if (
      nodes[map[i][0]].innerHTML == 'X' &&
      nodes[map[i][1]].innerHTML == 'X' &&
      nodes[map[i][2]].innerHTML == 'X'
    ) {
      isWin = true;
      colorWin([nodes[map[i][0]].id, nodes[map[i][1]].id, nodes[map[i][2]].id]);
      endGame();
    } else if (
      nodes[map[i][0]].innerHTML == 'O' &&
      nodes[map[i][1]].innerHTML == 'O' &&
      nodes[map[i][2]].innerHTML == 'O'
    ) {
      isWin = true;
      colorWin([nodes[map[i][0]].id, nodes[map[i][1]].id, nodes[map[i][2]].id]);
      endGame();
    }
  }
  if (moves === 9 && !isWin) {
    endGame();
  }
};

const nextPlayer = (player) => {
  if (player === 1) {
    player1 = true;
    player2 = false;
  } else if (player === 2) {
    player1 = false;
    player2 = true;
  }
};

const handleClick = (e) => {
  if (player1 && e.target.name !== 'marked') {
    player.innerHTML = `${player2 ? playerName1 : playerName2}`;
    e.target.innerHTML = 'X';
    e.target.name = 'marked';
    nextPlayer(2);
    moves = ++moves;
    count.innerHTML = `${moves}`;
  } else if (player2 && e.target.name !== 'marked') {
    player.innerHTML = `${player2 ? playerName1 : playerName2}`;
    e.target.innerHTML = 'O';
    e.target.name = 'marked';
    nextPlayer(1);
    moves = ++moves;
    count.innerHTML = `${moves}`;
  }
  winCheck();
};

for (let i = 0; i < nodes.length; i++) {
  const element = nodes[i];
  element.addEventListener('click', handleClick);
}

player.innerHTML = playerName1;
count.innerHTML = `${moves}`;
reset.addEventListener('click', resetClick);
